package com.openclassrooms.realestatemanager.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.openclassrooms.realestatemanager.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PropertyEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PropertyEditFragment extends Fragment {

    public PropertyEditFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PropertyEditFragment.
     */
    public static PropertyEditFragment newInstance() {
        return new PropertyEditFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.property_edit_fragment, container, false);
    }
}