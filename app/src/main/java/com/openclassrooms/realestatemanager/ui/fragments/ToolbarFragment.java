package com.openclassrooms.realestatemanager.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.openclassrooms.realestatemanager.R;
import com.openclassrooms.realestatemanager.enums.NavigationId;

public class ToolbarFragment extends Fragment {

    private Callbacks mCallback;

    public ToolbarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        mCallback = (ToolbarFragment.Callbacks) activity;
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking
        super.onDetach();
    }

    public interface Callbacks {
        void toolbarAttached();
        void onClickMenuItemToolbar(NavigationId navigationId);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ToolbarFragment.
     */
    public static ToolbarFragment newInstance() {
        return new ToolbarFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.toolbar_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCallback.toolbarAttached();

        ImageView mAddImageView = view.findViewById(R.id.add_toolbar_fragment);
        mAddImageView.setOnClickListener(addImage -> {
            mCallback.onClickMenuItemToolbar(NavigationId.ADD_PAGE);
        });

        ImageView mEditImageView = view.findViewById(R.id.edit_toolbar_fragment);
        mEditImageView.setOnClickListener(editImage -> {
            mCallback.onClickMenuItemToolbar(NavigationId.EDIT_PAGE);
        });
    }
}
