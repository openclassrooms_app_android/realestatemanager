package com.openclassrooms.realestatemanager.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.openclassrooms.realestatemanager.R;
import com.openclassrooms.realestatemanager.enums.NavigationId;
import com.simform.custombottomnavigation.SSCustomBottomNavigation;

public class BottomNavigationFragment extends Fragment {

    private final String LOG = BottomNavigationFragment.class.getSimpleName();

    private final int LISTING_ID = NavigationId.LISTING_PAGE.getNumericType();
    private final int LOAN_SIMULATOR_ID = NavigationId.LOAN_SIMULATOR_PAGE.getNumericType();
    private final String ACTIVE_PAGE_ID = "ACTIVE_PAGE_ID";

    private int activePageId;
    private SSCustomBottomNavigation bottomNavigation;

    private Callbacks mCallback;

    public BottomNavigationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BottomNavigationFragment.
     */
    public static BottomNavigationFragment newInstance() {
        return new BottomNavigationFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        mCallback = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking
        super.onDetach();
    }

    public interface Callbacks {
        void getBottomNavigationActivePageId(NavigationId navigationId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            activePageId = (int) savedInstanceState.get(ACTIVE_PAGE_ID);
        } else {
            activePageId = LISTING_ID;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bottom_navigation_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bottomNavigation = view.findViewById(R.id.custom_bottom_navigation);

        // Add link to bottom navigation menu
        bottomNavigation.add(new SSCustomBottomNavigation.Model(LISTING_ID, R.drawable.ic_listing,"Liste des biens"));
        bottomNavigation.add(new SSCustomBottomNavigation.Model(LOAN_SIMULATOR_ID, R.drawable.ic_loan_simulator,"Simulateur de prêt"));

        // Default fragment
        if(activePageId == LISTING_ID){
            bottomNavigation.show(LISTING_ID, true);
        } else if (activePageId == LOAN_SIMULATOR_ID) {
            bottomNavigation.show(LOAN_SIMULATOR_ID, true);
        }

        // Bottom Navigation click listener
        bottomNavigation.setOnClickMenuListener(model -> {
            NavigationId navigationId = null;
            switch (model.getId()) {
                case 0:
                    navigationId = NavigationId.LISTING_PAGE;
                    break;

                case 1:
                    navigationId = NavigationId.LOAN_SIMULATOR_PAGE;
                    break;
            }

            if(navigationId != null) {
                mCallback.getBottomNavigationActivePageId(navigationId);
                activePageId = model.getId();
            }

            return null;
        });

        // On change paqe the navigation bottom link is updated
        getParentFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment contentFragment = getParentFragmentManager().findFragmentById(R.id.container_content_activity_main);
            if(isInstanceOf(contentFragment, PropertyListingFragment.newInstance())) {
                bottomNavigation.show(NavigationId.LISTING_PAGE.getNumericType(), true);
            } else if(isInstanceOf(contentFragment, LoanSimulatorFragment.newInstance())) {
                bottomNavigation.show(NavigationId.LOAN_SIMULATOR_PAGE.getNumericType(), true);
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ACTIVE_PAGE_ID, activePageId);
    }

    public void removeActiveLink() {
//        bottomNavigation
    }

    public boolean isInstanceOf(Fragment fragment, Fragment targetFragment) {
        if(fragment == null || targetFragment == null) {
            return false;
        }
        return (fragment.getClass() == targetFragment.getClass());
    }

//    public void updateFragment(Fragment fragment, int enterAnim, int exitAnim) {
//        FragmentManager manager = getParentFragmentManager();
//        Fragment contentFragment = manager.findFragmentById(R.id.container_content_activity_property);
//        Fragment detailFragment = manager.findFragmentById(R.id.container_detail_activity_property);
//        FragmentTransaction transaction = manager.beginTransaction();
//
//        if(fragment instanceof PropertyListingFragment) {
//            activePageId = LISTING_ID;
//        } else if(fragment instanceof LoanSimulatorFragment) {
//            activePageId = LOAN_SIMULATOR_ID;
////            Log.d(LOG, "[LOG] --> LOAN SIMULATOR");
//        }
//        bottomNavigation.show(activePageId, true);
//
//        // Send active page id to activity
////        mCallback.getActivePageId(activePageId);
//
//        if(contentFragment != null) {
//            if(enterAnim != -1 && exitAnim != -1) {
//                transaction.setCustomAnimations(enterAnim, exitAnim);
//            }
//            transaction.replace(R.id.container_content_activity_property, fragment);
//        } else {
//            transaction.add(R.id.container_content_activity_property, PropertyListingFragment.newInstance(), "listing");
//        }
//
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
}
