package com.openclassrooms.realestatemanager.ui.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.openclassrooms.realestatemanager.R;
import com.openclassrooms.realestatemanager.Utils;

public class PropertyListingFragment extends Fragment {

    private final String LOG = PropertyListingFragment.class.getSimpleName();

    public PropertyListingFragment() {
    }

    public static PropertyListingFragment newInstance(){
        return new PropertyListingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.listing_fragment, container, false);
    }
}
