package com.openclassrooms.realestatemanager.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.openclassrooms.realestatemanager.R;
import com.openclassrooms.realestatemanager.enums.NavigationId;
import com.openclassrooms.realestatemanager.ui.fragments.BottomNavigationFragment;
import com.openclassrooms.realestatemanager.ui.fragments.ToolbarFragment;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements ToolbarFragment.Callbacks, BottomNavigationFragment.Callbacks {

    private static final String LOG = MainActivity.class.getSimpleName();

    private final String ACTIVE_NAVIGATION_ID = "ACTIVE_NAVIGATION_ID";
    private final String ENTER_ANIM = "ENTER_ANIM";
    private final String EXIT_ANIM = "EXIT_ANIM";

    private Resources resources;
    private FragmentManager manager;
    private Fragment mToolbarFragment;
    private ImageView mPreviousImageView;
    private FragmentContainerView mContentContainer;
    private Fragment mContentFragment;
    private FragmentContainerView mDetailContainer;
    private NavigationId activeNavigationId = NavigationId.LISTING_PAGE;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resources = getResources();
        manager = getSupportFragmentManager();

        if(savedInstanceState != null) {
            activeNavigationId = (NavigationId) savedInstanceState.get(ACTIVE_NAVIGATION_ID);
        }
        this.changeFragment(activeNavigationId, null);

        this.configurePage();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ACTIVE_NAVIGATION_ID, activeNavigationId);
    }

    @Override
    public void onBackPressed() {
        HashMap<String, Integer> animations = new HashMap<>();
        if(activeNavigationId == NavigationId.LISTING_PAGE) {
            finish();
            // TODO Open confirmation modal to exit application
        } else if(activeNavigationId == NavigationId.LOAN_SIMULATOR_PAGE){
            animations.put(ENTER_ANIM, R.anim.slide_in_left);
            animations.put(EXIT_ANIM, R.anim.slide_out_right);
        } else if(activeNavigationId == NavigationId.ADD_PAGE || activeNavigationId == NavigationId.EDIT_PAGE){
            animations.put(ENTER_ANIM, R.anim.zoom_in);
            animations.put(EXIT_ANIM, R.anim.zoom_out);
        }

        this.changeFragment(NavigationId.LISTING_PAGE, animations);
    }

    /** CUSTOM METHODS **/
    private void configurePage() {
        mContentContainer = findViewById(R.id.container_content_activity_main);
        mContentFragment = manager.findFragmentById(R.id.container_content_activity_main);

        if(resources.getBoolean(R.bool.isTabletInLandscapeOrientation)) {
            mDetailContainer = findViewById(R.id.container_detail_activity_main);
        }
    }

    public void changeFragment(NavigationId navigationId, HashMap<String, Integer> animations) {
        FragmentTransaction transaction = manager.beginTransaction();

        // If the fragment does not contain the requested fragment
        if(navigationId.getNumericType() != activeNavigationId.getNumericType()) {

            if(animations != null && animations.size() != 0) {
                int enterAnim = -1;
                int exitAnim = -1;
                for(Map.Entry<String, Integer> animation : animations.entrySet()) {
                    if(animation.getKey().equals(ENTER_ANIM)) {
                        enterAnim = animation.getValue();
                    }
                    if(animation.getKey().equals(EXIT_ANIM)) {
                        exitAnim = animation.getValue();
                    }
                }
                transaction.setCustomAnimations(enterAnim, exitAnim);
            } else {
                if(navigationId.getEnterAnim() != -1 && navigationId.getExitAnim() != -1) {
                    transaction.setCustomAnimations(navigationId.getEnterAnim(), navigationId.getExitAnim());
                }
            }

            // Check if fragment container is not empty
            if(activeNavigationId.getNumericType() >= 0) {
//                transaction.replace(R.id.container_content_activity_main, navigationId.getInstance());
                transaction.replace(R.id.container_content_activity_main, navigationId.getInstance(), navigationId.name());
            } else { // If fragment container is empty
                transaction.add(R.id.container_content_activity_main, navigationId.getInstance(), navigationId.name());
            }
            activeNavigationId = navigationId;

            transaction.addToBackStack(null);
            transaction.commit();

            this.setPreviousButton();
        }
    }

    public void setPreviousButton(){
        if(mToolbarFragment != null && mToolbarFragment.getView() != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
            Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
            if(activeNavigationId == NavigationId.LISTING_PAGE || activeNavigationId == NavigationId.LOAN_SIMULATOR_PAGE) {
                mPreviousImageView.setAnimation(fadeOut);
                mPreviousImageView.setVisibility(View.GONE);
            } else {
                mPreviousImageView.setAnimation(fadeIn);
                mPreviousImageView.setVisibility(View.VISIBLE);
            }
            mPreviousImageView.setOnClickListener(v -> this.onBackPressed());
        }
    }

    @Override
    public void toolbarAttached() {
        mToolbarFragment = manager.findFragmentById(R.id.container_toolbar_activity_main);

        if(mToolbarFragment != null && mToolbarFragment.getView() != null) {
            mPreviousImageView = mToolbarFragment.getView().findViewById(R.id.previous_page_toolbar_fragment);
            this.setPreviousButton();
        }
    }

    @Override
    public void onClickMenuItemToolbar(NavigationId navigationId) {
        // Change fragment
        // TODO Bug on clicked on add page icon, check the log error (INVALID ID 0x00000000)
        this.changeFragment(navigationId, null);
    }

    @Override
    public void getBottomNavigationActivePageId(NavigationId navigationId) {
        this.changeFragment(navigationId, null);
    }

    //    @Override
//    public void getActivePageId(int pageId) {
//        Log.d(LOG, "[LOG] --> getActivePageId : " + pageId);
//
//        int orientation = getResources().getConfiguration().orientation;
//        Float widthDimensionInDp = Utils.getDeviceDimensionInDp(this).get("width");
//        if(pageId == BottomNavigationId.LISTING_ID.getNumericType()) {
//            if(widthDimensionInDp != null) {
//                if(orientation == Configuration.ORIENTATION_LANDSCAPE && widthDimensionInDp >= 600 && Utils.isTablet(this)){
//                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(400, 0);
//                    mContentContainer.setLayoutParams(layoutParams);
//                    mDetailContainer.setVisibility(View.VISIBLE);
//                }
//            }
//        } else if(pageId == BottomNavigationId.LOAN_SIMULATOR_ID.getNumericType()) {
//            if(widthDimensionInDp != null) {
//                if(orientation == Configuration.ORIENTATION_LANDSCAPE && widthDimensionInDp >= 600 && Utils.isTablet(this)){
//                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
//                    mContentContainer.setLayoutParams(layoutParams);
//                    mDetailContainer.setVisibility(View.GONE);
//                }
//            }
//        }
//    }
}
