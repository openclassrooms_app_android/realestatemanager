package com.openclassrooms.realestatemanager.enums;

import com.openclassrooms.realestatemanager.R;
import com.openclassrooms.realestatemanager.ui.fragments.LoanSimulatorFragment;
import com.openclassrooms.realestatemanager.ui.fragments.PropertyAddFragment;
import com.openclassrooms.realestatemanager.ui.fragments.PropertyEditFragment;
import com.openclassrooms.realestatemanager.ui.fragments.PropertyListingFragment;

import androidx.fragment.app.Fragment;

public enum NavigationId {

    LISTING_PAGE(0, PropertyListingFragment.newInstance(), R.anim.slide_in_left, R.anim.slide_out_right),
    LOAN_SIMULATOR_PAGE(1, LoanSimulatorFragment.newInstance(), R.anim.slide_in_right, R.anim.slide_out_left),
    ADD_PAGE(2, PropertyAddFragment.newInstance(), R.anim.zoom_in, R.anim.zoom_out),
    EDIT_PAGE(3, PropertyEditFragment.newInstance(), R.anim.zoom_in, R.anim.zoom_out);

    private final int id;
    private final Fragment instance;
    private final int enterAnim;
    private final int exitAnim;
    NavigationId(int id, Fragment instance, int enterAnim, int exitAnim) {
        this.id = id;
        this.instance = instance;
        this.enterAnim = enterAnim;
        this.exitAnim = exitAnim;
    }

    public int getNumericType(){
        return id;
    }

    public Fragment getInstance() {
        return instance;
    }

    public int getEnterAnim() {
        return enterAnim;
    }

    public int getExitAnim() {
        return exitAnim;
    }
}
